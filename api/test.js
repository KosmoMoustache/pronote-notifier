#!/usr/bin/env node
require('dotenv').config();
const { login } = require('@dorian-eydoux/pronote-api');
const { getSummary } = require('releve');
const { PRONOTE_URL, PRONOTE_USERNAME, PRONOTE_PASSWORD } = process.env;

async function main() {
  const session = await login(PRONOTE_URL, PRONOTE_USERNAME, PRONOTE_PASSWORD);
  console.info(
    `Pronote client logged in as ${session.user.name} ${
      session.user.studentClass.name
    } ${new Date().toISOString()}`
  );
  console.info(getSummary(session));

  // Timetable experimentation
  Date.prototype.addDays = function (days) {
    const date = new Date(this.valueOf());
    date.setDate(date.getDate() + days);
    return date;
  };
  const date = new Date();
  date.addDays();
  const timetable = await session.timetable();
  console.log(timetable);
}

main().catch((err) => {
  console.error(err);
});
