const fetch = require('sync-fetch');
require('dotenv').config();
const {
  PRONOTE_URL,
  DEVS,
  DISCORD_AVATAR,
  DISCORD_PUBL_URL,
  DISCORD_PRIV_URL,
} = process.env;

async function sendWebhook(mark, subject, averages) {
  if (DEVS == 'true') console.debug({ mark }, { subject }, { averages });
  console.log('id: ', mark.id, '\nat: ', new Date().toISOString());

  const {
    average,
    max,
    min,
    coefficient,
    scale,
    title,
    isAway,
    value,
    id,
    date,
  } = mark;

  if (Object.entries(averages.glob).length == 0) {
    averages = {
      glob: {
        studentClass: 'x',
        student: 'x',
      },
      sub: {
        studentClass: 'x',
        student: 'x',
        new: [0, 0],
        old: [0, 0],
      },
    };
  }

  // Public
  const public_payload = {
    content: null,
    username: 'Pronote Notifier',
    avatar_url: `${DISCORD_AVATAR}`,
    content: '@everyone',
    embeds: [
      {
        title: `Nouvelle note en ${subject.name} ${title ? `(${title})` : ''}`,
        description: `Moyenne : ${average} / ${scale}\nNote+ : ${max}\nNote- : ${min}\nCoeff : ${coefficient}\n${
          isAway ? '> Devoir facultatif' : ''
        }`,
        url: `${PRONOTE_URL}`,
        color: parseInt(subject.color.slice(1), 16),
        fields: [
          {
            name: 'Évolution des moyennes',
            value: `Matière : ${averages.sub.studentClass}\nGénéral : ${averages.glob.studentClass}`,
          },
        ],
        footer: {
          text: `${id}`,
        },
        timestamp: `${new Date(date).toISOString()}`,
      },
    ],
  };
  // Private
  const private_payload = {
    content: null,
    username: 'Pronote Notifier',
    avatar_url: `${DISCORD_AVATAR}`,
    embeds: [
      {
        title: `(Priv) Nouvelle note en ${subject.name} (${title})`,
        description: `@everyone\nNote: ${value}\nMoyenne : ${average} / ${scale}\nNote+ : ${max}\nNote- : ${min}\nCoeff : ${coefficient}\n${
          isAway ? '> Devoir facultatif' : ''
        }`,
        url: `${PRONOTE_URL}`,
        color: parseInt(subject.color.slice(1), 16),
        fields: [
          {
            name: 'Évolution des moyennes',
            value: `Matière : ${averages.sub.studentClass}\nMoy.+ : ${averages.sub.new[0]} (${averages.sub.old[0]})\nMoy.- : ${averages.sub.new[1]} (${averages.sub.old[1]})\nGénéral : ${averages.glob.studentClass}`,
          },
          {
            name: 'Évolution perso',
            value: `Matière : ${averages.sub.student}\nGénéral : ${averages.glob.student}`,
          },
        ],
        footer: {
          text: `Priv ${id}`,
        },
        timestamp: `${new Date(date).toISOString()}`,
      },
    ],
  };

  // Public
  const fetchPublic = fetch(DISCORD_PUBL_URL, {
    method: 'POST',
    headers: {
      'Content-type': 'application/json',
    },
    body: JSON.stringify(public_payload),
  });

  // 429 Too many content -> Rate limit
  // 204 No Content -> OK
  if (fetchPublic.status != 204) rateLimit(public_payload, fetchPublic.headers);
  console.log('Status: ', fetchPublic.status);
  if (DEVS == 'true') printHeader(fetchPublic.headers);

  // Private
  const fetchPrivate = fetch(DISCORD_PRIV_URL, {
    method: 'POST',
    headers: {
      'Content-type': 'application/json',
    },
    body: JSON.stringify(private_payload),
  });

  // 429 Too many content -> Rate limit
  // 204 No Content -> OK
  if (fetchPrivate.status != 204) rateLimit(private_payload);
  console.log('Status: ', fetchPrivate.status);
  if (DEVS == 'true') printHeader(fetchPrivate.headers);
}

async function sendHomeworks(homeworks) {
  // console.log(homeworks);
  // console.log(`${homeworks.length} homeworks`, '\nat: ', new Date().toISOString());

  var list_embeds = [];

  var j = 0;
  var k = 0;

  for (let i = 0; i < homeworks.length; i++) {
    var work = homeworks[i];

    // Groupe les devoirs par 9
    // 9 = nombre max d'embeds dans un webhook
    // Quand j == 9, incrémente l'index de l'array list_embeds
    if (j == 9) {
      k++;
      j = 0;
    }

    // Declare qu'une fois l'array avec l'index k
    if (list_embeds[k] == undefined) list_embeds[k] = [];
    let fields = [];

    if (work.files != []) {
      work.files.forEach((file) => {
        fields.push({
          name: file.name,
          value: file.url,
        });
      });
    }

    list_embeds[k].push({
      title: `Nouveau devoir de ${work.subject}`,
      description: `^${work.description.replaceAll(/\r\n/g, '')}\nPour le ${
        new Date(work.for).toISOString().split('T')[0]
      }`,
      url: `${PRONOTE_URL}#${work.id}`,
      color: parseInt(work.color.slice(1), 16),
      fields: fields,
      footer: {
        text: `${work.id}`,
      },
      timestamp: `${new Date(work.givenAt).toISOString()}`,
    });

    j++;
  }

  list_embeds.forEach((embeds) => {
    const public_payload = {
      content: null,
      username: 'Pronote Notifier',
      avatar_url: `${DISCORD_AVATAR}`,
      content: '@everyone',
      embeds: embeds,
    };

    const fetchPublic = fetch(DISCORD_PUBL_URL, {
      method: 'POST',
      headers: {
        'Content-type': 'application/json',
      },
      body: JSON.stringify(public_payload),
    });

    // 429 Too many content -> Rate limit
    // 204 No Content -> OK
    if (fetchPublic.status != 204)
      rateLimit(public_payload, fetchPublic.headers);
    console.log('Status: ', fetchPublic.status);
    if (DEVS == 'true') printHeader(fetchPublic.headers);
  });
}

module.exports = {
  sendWebhook,
  sendHomeworks,
};

function rateLimit(data, headers) {
  console.error('\x1b[31m%s\x1b[0m', JSON.stringify(data));
  printHeader(headers);
}

// Log rate limit headers
function printHeader(headers) {
  let a = [
    'x-ratelimit-limit',
    'x-ratelimit-remaining',
    'x-ratelimit-reset',
    'x-ratelimit-reset-after',
  ];
  a.forEach((h) => {
    console.log(`${h} : ${headers.get(h)}`);
  });
}
