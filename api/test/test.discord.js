const { sendWebhook } = require('../discord');

(async () => {
    // Random hex color
    const hex = [
        '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F',
    ]
    var hexcolor = '#'
    for (let i = 0; hexcolor.length < 7; i++) {
        let rand = Math.floor((Math.random() * 15));
        hexcolor = hexcolor + hex[rand]
    }

    mark = {
        id: 'dfeaf2e877ef56ce',
        title: '4ème devoir',
        isAway: true,
        value: 16,
        min: 6,
        max: 16,
        average: 11.35,
        scale: 20,
        coefficient: 1,
        date: '2021-11-07T23:00:00.000Z'
    }
    subject = {
        name: 'ESPAGNOL LV2',
        averages: { student: 16.13, studentClass: 10.99, max: 16.13, min: 4 },
        color: `${hexcolor}`,
        marks: []
    }
    average = {
        glob: {
            studentClass: '=11.62 (11.62)',
            student: '=14.82 (14.82)'
        },
        sub: {
            studentClass: '=10.99 (10.99)',
            student: '=16.13 (16.13)',
            new: [10, 20],
            old: [10, 20]
        }
    }
    // Nombre de webhook à envoyer
    const recur = 50
    for (let i = 0; i < recur; i++) {
        console.log('\x1b[31m%s\x1b[0m', i)
        sendWebhook(mark, subject, average);
    }
})()
