#!/usr/bin/env node
require('dotenv').config();
const { login } = require('@dorian-eydoux/pronote-api');
const { JsonDB } = require('node-json-db');
const { Config } = require('node-json-db/dist/lib/JsonDBConfig');
const { sendWebhook, sendHomeworks } = require('./discord');
const { writeFileSync } = require('fs');

const { PRONOTE_URL, PRONOTE_USERNAME, PRONOTE_PASSWORD, PRONOTE_CAS, DEVS } =
  process.env;

// Arguments
if (process.argv.slice(2).indexOf('--help') >= 1) {
  process.stdout.write(
    'Arguements: \n--skip: will skip sending webhook to discord\n--nohomework will stop checking for new homework\n--nomarks will stop checking for new marks\n--notimetable will stop checking change in the timetable'
  );
  process.exit(0);
}

const skipWebhook = process.argv.slice(2).indexOf('--skip') != -1;
const skipHomework = process.argv.slice(2).indexOf('--nohomework') != -1;
const skipMarks = process.argv.slice(2).indexOf('--nomarks') != -1;
const skipTimetable = process.argv.slice(2).indexOf('--notimetable') != -1;
if (skipWebhook == true)
  console.log('--skip parameters: Skipping sending webhook to discord');
if (skipHomework == true)
  console.log('--nohomework parameters: Skipping checking for new homework');
if (skipMarks == true)
  console.log('--nomarks parameters: Skipping checking for new marks');
if (skipTimetable == true)
  console.log(
    '--notimetable parameters: Skipping checking for change in the timetable'
  );

// Config
const dbm = new JsonDB(new Config('./data/marks'));
const dba = new JsonDB(new Config('./data/averages'));
const dbw = new JsonDB(new Config('./data/homeworks'));
var flag_sbj = false;

(async () => {
  const session = await login(
    PRONOTE_URL,
    PRONOTE_USERNAME,
    PRONOTE_PASSWORD,
    PRONOTE_CAS
  );
  console.info(
    `Pronote client logged in as ${session.user.name} ${
      session.user.studentClass.name
    } ${new Date().toISOString()}`
  );

  if (skipTimetable == false) {
    console.log('qdsqdq');
    // fromDate = new Date();
    // toDate = new Date();
    // toDate.setDate(toDate.getDate() + 7);
    const timetable = await session.timetable(new Date(Date.now()));
    console.log(timetable);
  }

  if (skipHomework == false) {
    fromDate = new Date();
    toDate = new Date();
    toDate.setDate(toDate.getDate() + 365 / 2);

    const homeworks = await session.homeworks((from = fromDate), (to = toDate));

    var o_hids = getHIDS();
    var hids = [];
    if (DEVS == 'true') console.debug('H IDs from the db: ', o_hids);

    var homeworkArray = [];

    homeworks.map((homework) => {
      // homework.id n'est pas dans o_hids(=bdd courante) ===> nouveau devoir
      if (o_hids.indexOf(homework.id) == -1) homeworkArray.push(homework);
      hids.push(homework.id);
    });
    dbw.push('/ids', hids);
    if (hids.length > 0 && skipWebhook == false) sendHomeworks(homeworkArray);
  }

  if (skipMarks == false) {
    const marks = await session.marks((type = 'semester'));

    // Variable ids
    const ids = [];
    var averages = { glob: {}, sub: {} };

    var o_ids = getIDS();
    if (DEVS == 'true') console.debug('IDs from the db: ', o_ids);

    // averages: { student: 14.82, studentClass: 11.62 }
    let o_avg = getAvg('/globalAverage/');
    if (DEVS == 'true') console.debug('Global averages from db: ', o_avg);

    // Met à jour la base de donnée si nécessaire
    if (
      Object.entries(o_avg).length == 0 ||
      o_avg[0].studentClass != marks.averages.studentClass
    ) {
      // Ajoute une entrée a l'historique
      marks.averages.date = new Date();
      dba.push('/globalAverage[]', marks.averages, true);
    }

    if (Object.entries(o_avg).length != 0) {
      // Compare les moyennes courante et précédente
      averages.glob = { ...compareAverage(marks.averages, o_avg[0]) };
    }

    marks.subjects.map((subject) => {
      // Récupère les précédente moyennes matière
      // averages: { student: 16, studentClass: 13.56, max: 20, min: 2 },

      let SubjectName = subject.name.replace(/[^a-zA-Z0-9]/g, '').toLowerCase();
      if (SubjectName == 'informatiquereseauxinformatiquereseaux') {
        if (!flag_sbj) {
          SubjectName += '_1';
          flag_sbj = true;
        } else {
          SubjectName += '_2';
        }
      }

      let o_avg = getAvg(`/subAverage/${SubjectName}`);
      if (DEVS == 'true') console.debug('Subject averages from db: ', o_avg);

      // Met à jour la base de donnée si nécessaire
      if (
        Object.entries(o_avg).length == 0 ||
        o_avg[0].studentClass != subject.averages.studentClass
      ) {
        // Ajoute une entrée a l'historique
        subject.averages.date = new Date();
        dba.push(`/subAverage/${SubjectName}[]`, subject.averages, true);
      }

      // Compare les moyennes courante et précédente
      if (Object.entries(o_avg).length != 0) {
        averages.sub = {
          ...compareAverage(subject.averages, o_avg[0]),
          new: [subject.averages.max, subject.averages.min],
          old: [o_avg[0].max, o_avg[0].min],
        };
      }

      subject.marks.map((mark) => {
        // mark.id n'est pas dans o_ids(=bdd courante) ===> nouvelle note
        subject.marks = null;
        if (o_ids.indexOf(mark.id) == -1 && skipWebhook == false)
          sendWebhook(mark, subject, averages);
        ids.push(mark.id);
      });
    });

    // Ajoute les ids dans bdd
    dbm.push('/ids', ids);
  }

  process.exit(0);
})();

// Récupère les IDs
function getIDS() {
  try {
    return dbm.getData('/ids');
  } catch {
    writeFileSync('./data/marks.json', '[]');
    return [];
  }
}

// Récupère les IDs
function getHIDS() {
  try {
    return dbw.getData('/ids');
  } catch {
    writeFileSync('./data/homeworks.json', '[]');
    return [];
  }
}

/**
 * Récupère les moyennes avec le nom de la matière en paramètre
 * subject.replace(/[^a-zA-Z0-9]/g, "").toLowerCase(
 */
function getAvg(path) {
  try {
    return dba.getData(path).reverse();
  } catch {
    // Desfois ça fonctionne
    writeFileSync('./data/averages.json', '[]');
    return [];
  }
}

// Compare les moyennes et formate l'affichage
function compareAverage(now, old) {
  let avgs = {};
  // class
  if (now.studentClass > old.studentClass) {
    avgs.studentClass = `➚${now.studentClass} (${old.studentClass})`;
  } else if (now.studentClass < old.studentClass) {
    // <
    avgs.studentClass = `➘${now.studentClass} (${old.studentClass})`;
  } else {
    avgs.studentClass = `=${now.studentClass} (${old.studentClass})`;
  }
  // student
  if (now.student > old.student) {
    avgs.student = `➚${now.student} (${old.student})`;
  } else if (now.student < old.student) {
    // <
    avgs.student = `➘${now.student} (${old.student})`;
  } else {
    avgs.student = `=${now.student} (${old.student})`;
  }
  return avgs;
}
