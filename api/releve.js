const { navigate } = require("@dorian-eydoux/pronote-api");
const { getPeriodBy } = require("@dorian-eydoux/pronote-api/src/data/periods");
const { toPronote } = require('@dorian-eydoux/pronote-api/src/data/objects');

async function getSummary(session) {
    const PAGE_NAME = 'PageReleve';
    const TAB_ID = 12;
    const ACCOUNTS = ['student', 'parent'];

    const period = getPeriodBy(session, null, 'semester')

    const summary = await navigate(session, session.user, PAGE_NAME, TAB_ID, ACCOUNTS, {
        periode: { ...toPronote(period), G: 2 }, eleve: toPronote(session.user)
    })

    if (!summary.Message) {
        return {
            average: {
                student: summary.General.V.MoyenneEleve.V,
                studentClass: summary.General.V.MoyenneClasse.V,
                median: summary.General.V.MoyenneMediane.V,
                max: summary.General.V.MoyenneSup.V,
                min: summary.General.V.MoyenneInf.V,
            },
        }
    } else {
        return null
    }

}

module.exports = {
    getSummary
}