const path = {
  log: 'path/to/log',
  pronote_dir: 'path/ro/pronote_dir',
};

const portainer = {
  username: '',
  password: '',
  url: '',
  container_id: '',
};

module.exports = {
  path,
  portainer,
};
