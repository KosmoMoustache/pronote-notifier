const { watch, readFile } = require('fs');
const EventEmitter = require('events');
const LogChange = new EventEmitter();
const { path } = require('./config');

/**
 * Check for change on the filename
 * and fire a 'change' event for 'LogChange' with the date read if the file
 */
watch(path.log, (eventType, filename) => {
  setTimeout(() => {
    readFile(path.log, (err, data) => {
      // Récupère la date dans le fichier log
      LogChange.emit(
        'change',
        new Date(Date(Buffer.from(data).toString().split(' ').pop())).getTime()
      );
    });
  }, 500);
});

module.exports = {
  LogChange,
};
