const fetch = require('node-fetch');
const { portainer } = require('./config');

/**
 * Everything here use the portainer api
 */

// Cache
var cache;
cache = {
    date: 0,
    jwt: ''
}

/**
 * Gather a jwt
 * @param {string} username 
 * @param {string} password 
 * @returns {jwt}
 * @private
 */
async function getJWT(username, password) {
    const rawResponse = await fetch(`${portainer.url}/api/auth`, {
        method: 'POST',
        headers: {
            "Content-Type": "application/json"
        },
        body: JSON.stringify({
            username: username,
            password: password
        })
    })

    const content = await rawResponse.json()
    return content.jwt
}

/**
 * Restart the container
 * @retruns {string}
 */
async function RestartContainer() {
    const UpdateJWT = async () => {
        let jwt
        // JWT in the cache is valid
        if (Date.now() > cache.date) {
            // Get from api and update cache
            jwt = await getJWT(portainer.username, portainer.password)
            cache.date = JSON.parse(Buffer.from(jwt.split('.')[1], 'base64')).exp * 1000
            cache.jwt = jwt
        } else {
            // Use cache
            jwt = cache.jwt
        }
        return jwt
    }

    // Get jwt
    var jwt = await UpdateJWT();
    // Restart container
    const rawResponse = await fetch(`${portainer.url}/api/endpoints/1/docker/containers/${portainer.container_id}/restart`, {
        method: 'POST',
        headers: {
            "Authorization": `Bearer ${jwt}`
        }
    })

    if (rawResponse.status == 404) {
        return 'Conteneur redémarré!'
    } else {
        return `Erreur, status: ${rawResponse.status}`
    }

}

module.exports = {
    RestartContainer
}