//
const { Client, Intents, MessageEmbed } = require('discord.js');
const { execSync } = require('child_process');
const moment = require('moment');
//
const { LogChange } = require('./event');
const { RestartContainer } = require('./portainer');
const { CommandsCreate } = require('./registerCommands');
// Config
const { path } = require('./config');
const path = require('path');
require('dotenv').config();
const { BOT_TOKEN } = process.env;

const client = new Client({
  intents: [
    Intents.FLAGS.GUILDS,
    // guildMemberAdd event
    Intents.FLAGS.GUILD_MEMBERS,
  ],
});

/**
 * The function checks if the timeout has been reached. If it has, it calls the callback function. If
 * not, it sends a message to the user with the remaining timeout
 * @param interaction - The interaction object that contains the message that triggered the bot.
 * @param callback - The function to be called when the timeout is reached.
 */
var timeout = Date.now();
const HandleTimeout = (interaction, callback) => {
  /**
   * Check if the timeout has been reached
   * @returns a boolean value.
   */
  const checkTimeout = () => {
    if (timeout < Date.now()) {
      return true;
    } else {
      return false;
    }
  };

  if (checkTimeout()) {
    callback();
    timeout = Date.now() + 60 * 1000;
  } else {
    interaction.reply({
      content: `Timeout de ${Math.round(
        (timeout - Date.now()) / 1000
      )} secondes`,
      ephemeral: false,
    });
    // Update replay each seconds with the remaining timeout
    // interval = setInterval(() => {
    //   if (timeout - Date.now() < 0) clearInterval(interval)
    //   interaction.editReply({ content: `Timeout de ${Math.round((timeout - Date.now()) / 1000)} secondes`, ephemeral: false })
    // }, 1000)
    // (interaction.deleteReply())
  }
};

/**
 * Watch change on the log file
 * and change the bot activity
 */
LogChange.on('change', (time) => {
  console.log('New update: ', time);
  UpdateActivity(moment(new Date(time)).format('HH:mm DD/MM'));
});

/**
 * Change the activity of the bot
 * Type WATCHING
 * @param {string} activity - The activity to be displayed
 * @param {string} [type=WATCHING] - Type of the activity (see: https://discord.js.org/#/docs/discord.js/stable/typedef/ActivityType)
 */
const UpdateActivity = (activity, type) => {
  // if type exist
  // if type is in the accepted value
  // if not use 'WATCHING' as type
  type = type
    ? [
        'PLAYING',
        'STREAMING',
        'LISTENING',
        'WATCHING',
        'CUSTOM',
        'COMPETING',
      ].indexOf(type) > 0
      ? type
      : 'WATCHING'
    : 'WATCHING';
  client.user.setActivity(activity, { type: type });
};

/**
 * Fired when the bot is ready
 * Register its commands
 * Update activity
 * Update roles
 */
client.on('ready', async () => {
  console.log(`Logged in as ${client.user.tag}!`);

  CommandsCreate();
  UpdateActivity('🦦');
});

/**
 * Command handler
 */
client.on('interactionCreate', async (interaction) => {
  if (!interaction.isCommand()) return;

  // Update #pronote by running ./start in PRONOTE_DIR
  if (interaction.commandName === 'update') {
    HandleTimeout(interaction, async () => {
      await interaction.deferReply({ ephemeral: true });

      try {
        // Execute 'start' dans path.pronote_dir
        stdout = Buffer.from(
          execSync('./start', { cwd: path.pronote_dir })
        ).toString();
        console.log('Mise à jour de #pronote: ', stdout);
        await interaction.editReply(stdout);
        UpdateActivity(moment(new Date()).format('hh:mm DD/MM'));
      } catch (error) {
        console.log(
          'Erreur lors de la mise à jour de #pronote: ',
          error.message
        );
        await interaction.editReply(error.message);
        UpdateActivity('❗❗ Erreur ❗❗');
      }
    });
  }

  // Restart music bot
  if (interaction.commandName === 'music') {
    HandleTimeout(interaction, async () => {
      await interaction.reply({
        content: 'Redémarrage du bot music...',
        ephemeral: false,
      });
      await interaction.editReply(await RestartContainer());
    });
  }
});

client.login(BOT_TOKEN);
