const { SlashCommandBuilder } = require('@discordjs/builders');
const { REST } = require('@discordjs/rest');
const { Routes } = require('discord-api-types/v9');
const { guilds } = require('./config');
require('dotenv').config();

const { CLIENT_ID, BOT_TOKEN } = process.env;

/**
 * Register commands to discord
 */
const CommandsCreate = () => {
  // Commands
  const commands = [
    new SlashCommandBuilder()
      .setName('music')
      .setDescription('Redémarre le bot music'),
    new SlashCommandBuilder()
      .setName('update')
      .setDescription('Update #pronote'),
  ].map((command) => command.toJSON());

  const rest = new REST({ version: '9' }).setToken(BOT_TOKEN);

  // Register to guild
  // rest.put(Routes.applicationGuildCommands(CLIENT_ID, guilds.main), { body: commands })
  // Resgister globally
  rest
    .put(Routes.applicationCommands(CLIENT_ID), { body: commands })
    .then(() => console.log('Successfully registered application commands.'))
    .catch(console.error);
};

const CommandsDelete = () => {
  const rest = new REST({ version: '9' }).setToken(BOT_TOKEN);
  // Get from guild
  // rest.get(Routes.applicationGuildCommands(CLIENT_ID, guilds.main))
  // Get globally
  rest.get(Routes.applicationCommands(CLIENT_ID, guilds.main)).then((data) => {
    const promises = [];
    for (const command of data) {
      // Delete from guild
      // const deleteUrl = `${Routes.applicationGuildCommands(CLIENT_ID, guilds.main)}/${command.id}`;
      // Delete globally
      const deleteUrl = `${Routes.applicationCommands(CLIENT_ID)}/${
        command.id
      }`;
      promises.push(rest.delete(deleteUrl));
    }
    return Promise.all(promises)
      .then(() => console.log(`Successfully deleted application commands.`))
      .catch(console.error);
  });
};
module.exports = {
  CommandsCreate,
  CommandsDelete,
};
