# Crontab :

```bash
5,30 0,1,7-21 * 1-6,9-12 * cd /path/to/the/directory; ./index.js >> /path/to/the/directory/logs/cron-`date +\%F`.log
```

! Penser à chmod index.js pour pouvoir l'executer

À 5 et 30 minutes,
des heures entre 7h et 21h et 0 et 1 heure,
tout les jours des mois,
de Janvier à Juillet et de Septembre à Décembre,
tout les jours de la semaine

atrium-sud.js patché:
https://pastebin.com/td3ZCwLh
