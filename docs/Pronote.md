(Les valeurs par défaut pointent vers l'espace démo de indexeducation)

### username : Identifiant de votre compte Pronote
    - Défaut : `demonstration`
### password : Mot de passe de votre compte Pronote
    - Défaut : `pronotevs`
### url : Url de votre espace pronote
    - Défaut : [`https://demo.index-education.net/pronote/`](https://demo.index-education.net/pronote/)

### cas : [Voir @dorian-eydoux/pronote-api](https://www.npmjs.com/package/@dorian-eydoux/pronote-api#user-content-comptes-r%C3%A9gion-support%C3%A9s) ou lien en bas de page
    - Si pas de cas mettre `none`
    - Défaut : `none`